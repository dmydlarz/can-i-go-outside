package pl.sprawnyprogramista;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CanigooutsideApplication {

	public static void main(String[] args) {
		SpringApplication.run(CanigooutsideApplication.class, args);
	}
}
